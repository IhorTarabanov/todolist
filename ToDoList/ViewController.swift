//
//  ViewController.swift
//  ToDoList
//
//  Created by Tarabanov Igor on 19.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import UIKit

let DefaultCellIdentifier = "DefaultCellIdentifier"
let AddTodoSegue = "AddTodoSegue"
let ShowDetail = "ShowDetail"

let SegmentControlActiveTodos = 0
let SegmentControlDoneTodos = 1

class ViewController: UIViewController {
    @IBOutlet weak var segmentOutlet: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var todoProvider = ToDoProvider.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }


    @IBAction func addToDoClicked(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: AddTodoSegue , sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addTodoController = segue.destination as? AddToDoViewController{
            addTodoController.rootDelegate = self
        }else if let detailView = segue.destination as? DetailViewController, let todo = sender as? Todo{
            detailView.toDoLink = todo
            detailView.detailDelegate = self
        }
        
    }
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
}

extension ViewController: AddTodoControllerDelegate, DetailControllerDelegate {
    func checkStatus(todo: Todo) {
        let newValue = todo.isDone
        todo.isDone = todoProvider.statusIsDone(boolStatus: newValue)
        self.tableView.reloadData()
    }
    
   
    func todoDidCreate(_ todo: Todo) {
        //
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func selectedTodos() -> [Todo] {
        return self.segmentOutlet.selectedSegmentIndex == SegmentControlDoneTodos ? todoProvider.doneTodos : todoProvider.activeTodos
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectedTodos().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DefaultCellIdentifier, for: indexPath)
        let todo = self.selectedTodos()[indexPath.row]
        cell.textLabel?.text = todo.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else {return}
        
        
        let activeList = selectedTodos()
        let todo = activeList[indexPath.row]
        
        todoProvider.removeTodo(todo: todo)

        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = self.selectedTodos()[indexPath.row]
        self.performSegue(withIdentifier: ShowDetail, sender: selected)
    }
}



