//
//  AddToDoViewController.swift
//  ToDoList
//
//  Created by Tarabanov Igor on 19.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import UIKit

protocol AddTodoControllerDelegate: class {
    func todoDidCreate(_ todo: Todo) -> Void
}

class AddToDoViewController: UIViewController {
    
    weak var rootDelegate: AddTodoControllerDelegate?
    
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.delegate = self
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .green
        // Do any additional setup after loading the view.
    }
  
    @IBAction func addTodoClicked(_ sender: UIButton) {
        
        let text = self.textField.text ?? ""
        let todo = ToDoProvider.shared.addTodo(str: text)
        
        // notify delegate:
        self.rootDelegate?.todoDidCreate(todo)
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension AddToDoViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return ((textField.text?.count ?? 0) + string.count) <= 200
    }
}
