//
//  ToDoProvider.swift
//  ToDoList
//
//  Created by Tarabanov Igor on 19.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import Foundation

class Todo: Equatable {
    static func == (lhs: Todo, rhs: Todo) -> Bool {
        return lhs.title == rhs.title && lhs.createAt == rhs.createAt
    }

    var title: String
    var createAt: Date
    var isDone: Bool = false
    
    init() {
        self.title = ""
        self.createAt = Date.init()
    }
    
    init(title: String, isDone: Bool = false) {
        self.title = title
        self.isDone = isDone
        self.createAt = Date.init()
    }
    
}

public class ToDoProvider {
    var todoArray: [Todo] = []
    
    var activeTodos: [Todo] {
        return self.todoArray.filter { $0.isDone == false}
    }
    
    var doneTodos: [Todo] {
        return self.todoArray.filter { $0.isDone == true}
    }
    
    
    static let shared = ToDoProvider()
    
    fileprivate init() {
        self.todoArray = [
            Todo(title: "Todo No 1"),
            Todo(title: "Second todo"),
            Todo(title: "Third todo", isDone: true),
            Todo(title: "Fourth todo"),
            Todo(title: "Fifth todo")
        ]
    }
    
    func addTodo(str: String) -> Todo {
        let todo = Todo()
        todo.title = str
        todoArray.append(todo)
        return todo
    }
    
    func removeTodo(todo: Todo) -> Void {
        if let idx = self.todoArray.firstIndex(of: todo) {
            self.todoArray.remove(at: idx)
        }
    }
    
    func statusIsDone(boolStatus: Bool)-> Bool{
        return !boolStatus
    }
}
