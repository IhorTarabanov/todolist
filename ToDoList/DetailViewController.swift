//
//  DetailViewController.swift
//  ToDoList
//
//  Created by Tarabanov Igor on 20.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import UIKit

protocol DetailControllerDelegate: class {
    func checkStatus(todo: Todo)
}

class DetailViewController: UIViewController {
    
    weak var detailDelegate: DetailControllerDelegate?
    
    var toDoLink: Todo?
    
    @IBOutlet weak var todoLabel: UILabel!
    @IBOutlet weak var buttonOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todoLabel.text = toDoLink?.title
        buttonOutlet.setTitle("isisisi", for: .normal)
        if let trueCheck = toDoLink?.isDone {
            switch trueCheck {
            case true:
                buttonOutlet.setTitle("unDone", for: .normal)
            case false:
                buttonOutlet.setTitle("isDone", for: .normal)
            }
        }
    }
    

    @IBAction func isDoneButton(_ sender: UIButton) {
        
        if let newStatus = self.detailDelegate?.checkStatus(todo: toDoLink!){
            
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}
